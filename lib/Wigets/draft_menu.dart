import 'package:backend1/Wigets/Customder.dart';
import 'package:flutter/material.dart';

class draft_menu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          brightness: Brightness.light,
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(
            color: Theme.of(context).primaryColor,
          ),
          title: Text(
            'Đơn nháp',
            style: TextStyle(
              color: Theme.of(context).primaryColor,
              fontWeight: FontWeight.bold,
            ),
          ),
          actions: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                IconButton(
                    icon: const Icon(Icons.shopping_cart), onPressed: () {}),
                IconButton(
                    icon: const Icon(Icons.notifications),
                    tooltip: 'Increase volume by 10',
                    onPressed: () {}),
              ],
            ),
          ]),
      drawer: CustomDrawer(),
      body: ListView(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(5.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Expanded(
                  flex: 8,
                  child: TextField(
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.symmetric(vertical: 15.0),
                      fillColor: Colors.white,
                      filled: true,
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(15.0),
                        borderSide: BorderSide(width: 0.8),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(15.0),
                        borderSide: BorderSide(
                          color: Theme.of(context).primaryColor,
                        ),
                      ),
                      hintText: 'Search',
                      prefixIcon: Icon(
                        Icons.search,
                        size: 30.0,
                        color: Colors.green,
                      ),
                    ),
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Container(
                    child: IconButton(
                      icon: const Icon(Icons.filter_alt_outlined),
                      iconSize: 40.0,
                      onPressed: () {},
                    ),
                  ),
                ),
              ],
            ),
          ),
          Align(
            alignment: Alignment
                .topCenter, // Align however you like (i.e .centerRight, centerLeft)
            child: Text("Danh sách có 30 đơn"),
          ),
          Container(
            height: 500.0,
            child: ListView(
              scrollDirection: Axis.vertical,
              children: <Widget>[
                for(int i=0;i <10;i++)
                Card(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text('1.D022102003'),
                          ),
                          Text('Đơn nháp'),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Padding(
                            padding: EdgeInsets.fromLTRB(8.0, 0.0, 8.0, 8.0),
                            child: Row(children: <Widget>[
                              Icon(
                                Icons.person_outline,
                              ),
                              Text(
                                "Nguyễn Ngọc Đức",
                              ),
                            ]),
                          ),
                          Padding(
                            padding: EdgeInsets.fromLTRB(8.0, 0.0, 8.0, 8.0),
                            child: Row(children: <Widget>[
                              Icon(
                                Icons.phone,
                              ),
                              Text(
                                "0989 898 989",
                              ),
                            ]),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
