import 'package:backend1/Wigets/Customder.dart';
import 'package:flutter/material.dart';

class product_details extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
     return Scaffold(
                        appBar: AppBar(
                          backgroundColor: Colors.white,
                          
                          title: Text(
                            '1X jodan 4 kem(40)',
                            style: TextStyle(
                              color: Theme.of(context).primaryColor,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          actions: <Widget>[
                            FlatButton(
                              child: const Icon(Icons.close),
                              onPressed: () => Navigator.pop(context),
                            ),
                          ],
                        ),
                        body: Container(
                          height: MediaQuery.of(context).size.height,
                          child: ListView(
                            children: <Widget>[
                              Card(
                                child: Column(
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Row(
                                        children: [
                                          Row(
                                            children: [
                                              Image.asset(
                                                "assets/images/sanpham.PNG",
                                                fit: BoxFit.contain,
                                              )
                                            ],
                                          ),
                                          Column(
                                            children: [
                                              Wrap(
                                                children: [
                                                  Padding(
                                                    padding:
                                                        EdgeInsets.fromLTRB(
                                                            0.0, 0.0, 8.0, 0.0),
                                                    child: Text('Jodan 4 kem',
                                                        style: TextStyle(
                                                          color: Colors.green,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                        )),
                                                  ),
                                                ],
                                              ),
                                              SizedBox(height: 5.0),
                                              Wrap(
                                                children: [
                                                  Padding(
                                                    padding:
                                                        EdgeInsets.fromLTRB(
                                                            0.0, 0.0, 8.0, 0.0),
                                                    child: Text(
                                                        '660.000đ(ctv 0đ)'),
                                                  ),
                                                ],
                                              ),
                                              Wrap(
                                                children: [
                                                  Padding(
                                                    padding:
                                                        EdgeInsets.fromLTRB(
                                                            0.0, 0.0, 8.0, 0.0),
                                                    child: Text('size:40'),
                                                  ),
                                                  Text('số lượng:1'),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Row(
                                        children: [
                                          Padding(
                                            padding: EdgeInsets.all(10.0),
                                            child: Text(
                                              'Thông tin đơn hàng',
                                              style: TextStyle(
                                                fontSize: 20.0,
                                              ),
                                              textAlign: TextAlign.center,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Text(
                                            'Ngày tạo',
                                            style: TextStyle(
                                              fontSize: 15.0,
                                            ),
                                            textAlign: TextAlign.center,
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.all(8.0),
                                          child: Text(
                                            '9:31 18/03/21',
                                            style: TextStyle(
                                              fontSize: 15.0,
                                            ),
                                            textAlign: TextAlign.center,
                                          ),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Text(
                                            'Cập nhật cuối',
                                            style: TextStyle(
                                              fontSize: 15.0,
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.all(8.0),
                                          child: Text(
                                            '9:31 27/03/21',
                                            style: TextStyle(
                                              fontSize: 15.0,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Text(
                                            'Mã đơn',
                                            style: TextStyle(
                                              fontSize: 15.0,
                                            ),
                                          ),
                                        ),
                                        Row(children: <Widget>[
                                          Text("D03211804",
                                              style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                              )),
                                          Icon(
                                            Icons.copy,
                                            color: Colors.green,
                                          ),
                                        ])
                                      ],
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Text(
                                            'Vận đơn',
                                            style: TextStyle(
                                              fontSize: 15.0,
                                            ),
                                          ),
                                        ),
                                        Row(children: <Widget>[
                                          Icon(
                                            Icons.copy,
                                            color: Colors.green,
                                          ),
                                        ])
                                      ],
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Text(
                                            'Phí ship',
                                            style: TextStyle(
                                              fontSize: 15.0,
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Text(
                                            '20.000đ',
                                            style: TextStyle(
                                              fontSize: 15.0,
                                            ),
                                          ),
                                        ),
                                       
                                      ],
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Text(
                                            'Tổng đơn',
                                            style: TextStyle(
                                              fontSize: 15.0,
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Text(
                                            '680.000đ',
                                            style: TextStyle(
                                              fontSize: 15.0,
                                            ),
                                          ),
                                        ),
                                       
                                      ],
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Text(
                                            'Lãi dự kiến',
                                            style: TextStyle(
                                              fontSize: 15.0,
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Text(
                                            '660.000đ',
                                            style: TextStyle(
                                              fontSize: 15.0,
                                            ),
                                          ),
                                        ),
                                       
                                      ],
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Text(
                                            'Trạng thái đơn',
                                            style: TextStyle(
                                              fontSize: 15.0,
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.all(8.0),
                                           
                                            child: Container(
                                              child: Text(
                                              'Đã hủy',
                                              style: TextStyle(
                                                fontSize: 15.0,
                                                
                                                ),
                                                 
                                              ),
                                             decoration: BoxDecoration(
                    color: Colors.green,
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                                            ),
                                            
                                          ),
                                          
                                        
                                       
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                               Align(
                                alignment: Alignment
                                    .centerLeft, // Align however you like (i.e .centerRight, centerLeft)
                                child: Text(
                                  "Cộng tác viên",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                              ),
                               Card(
                                child: Column(
                                  children: <Widget>[
                                    Padding(
                                      padding: EdgeInsets.all(8.0),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            'Tên cộng tác viên',
                                            style: TextStyle(),
                                          ),
                                          Text(
                                            'Admintrator23',
                                            style: TextStyle(),
                                          ),
                                        ],
                                      ),
                                    ),
                                   
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            'Điện thoại CTV',
                                            style: TextStyle(
                                              fontSize: 15.0,
                                            ),
                                            textAlign: TextAlign.center,
                                          ),
                                          Container(
                                            child: RaisedButton.icon(
                                              onPressed: () {},
                                              icon: Icon(Icons.phone),
                                              label: Text('0389033651'),
                                              color: Colors.green,

                                            ),
                                            decoration: BoxDecoration(
                    color: Colors.green,
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                                          )
                                        ],
                                      ),
                                    ),
                                  
                                  ],
                                ),
                              ),
                              Align(
                                alignment: Alignment
                                    .centerLeft, // Align however you like (i.e .centerRight, centerLeft)
                                child: Text(
                                  "Người mua hàng",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                              ),
                              Card(
                                child: Column(
                                  children: <Widget>[
                                    Padding(
                                      padding: EdgeInsets.all(8.0),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            'Tên khách',
                                            style: TextStyle(),
                                          ),
                                          Text(
                                            'A Giang',
                                            style: TextStyle(),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Row(
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.all(5.0),
                                          child: Text(
                                            'Địa chỉ:',
                                            style: TextStyle(
                                              fontSize: 15.0,
                                            ),
                                            textAlign: TextAlign.center,
                                          ),
                                        ),
                                        Text(
                                          '123,Phú Sơn ,Kỳ Minh,Hòa Bình',
                                          style: TextStyle(
                                            fontSize: 15.0,
                                          ),
                                          textAlign: TextAlign.center,
                                        ),
                                      ],
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(5.0),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            'Điện thoại CTV',
                                            style: TextStyle(
                                              fontSize: 15.0,
                                            ),
                                            textAlign: TextAlign.center,
                                          ),
                                          Container(
                                            child: RaisedButton.icon(
                                              onPressed: () {},
                                              icon: Icon(Icons.phone),
                                              label: Text('0389033651'),
                                              color: Colors.green,
                                            ),
                                            decoration: BoxDecoration(
                    color: Colors.green,
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                                            
                                          )
                                        ],
                                      ),
                                    ),
                                    Row(
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Text(
                                            'Ghi chú cho khách và shipper',
                                            style: TextStyle(
                                              fontSize: 15.0,
                                            ),
                                            textAlign: TextAlign.center,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              Align(
                                alignment: Alignment
                                    .centerLeft, // Align however you like (i.e .centerRight, centerLeft)
                                child: Text(
                                  "Vận chuyển",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                              ),
                              Card(
                                child: Column(
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            'Đang chọn(click để...',
                                            style: TextStyle(
                                              fontSize: 15.0,
                                            ),
                                            textAlign: TextAlign.center,
                                          ),
                                          Container(
                                            child: RaisedButton.icon(
                                              onPressed: () {},
                                              icon: Icon(Icons.flight),
                                              label: Text('VIETELPOST'),
                                              color: Colors.yellow,
                                            ),
                                            decoration: BoxDecoration(
                    color: Colors.green,
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                                          )

                                        ],
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            'Vận chuyển',
                                            style: TextStyle(
                                              fontSize: 15.0,
                                            ),
                                            textAlign: TextAlign.center,
                                          ),
                                          Text(
                                            'LCOD',
                                            style: TextStyle(
                                              fontSize: 15.0,
                                            ),
                                            textAlign: TextAlign.center,
                                          ),
                                        ],
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            'Quá trình Vận chuyển',
                                            style: TextStyle(
                                              fontSize: 20.0,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            'Trạng thái thu gom',
                                            style: TextStyle(
                                              fontSize: 15.0,
                                            ),
                                          ),
                                          Text(
                                            'Chưa thu gom',
                                            style: TextStyle(
                                              fontSize: 15.0,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            'Thông tin giao nhận cuối cùng',
                                            style: TextStyle(),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            'Giao nhận lần cuối',
                                            style: TextStyle(),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Align(
                                alignment: Alignment
                                    .centerLeft, // Align however you like (i.e .centerRight, centerLeft)
                                child: Text(
                                  "Đường đi của vận đơn",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                              ),
                            ],
                          ),
                        ),
                      );
  }
}
Widget RenderListWidgetsHospital(BuildContext context)
  {
    return Scaffold(
      appBar: AppBar(
        brightness: Brightness.light,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(
          color: Theme.of(context).primaryColor,
        ),
        title: Text(
          'Đơn bị hủy',
          style: TextStyle(
            color: Theme.of(context).primaryColor,
            fontSize: 20.0,
            fontWeight: FontWeight.bold,
          ),
        ),
        actions: <Widget>[
          FlatButton(
            child: Row(
              children: [
                Icon(Icons.shopping_cart),
              ],
            ),
            onPressed: () {},
          )
        ],
      ),
      drawer: CustomDrawer(),
      body: ListView(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(5.0),
            child: TextField(
              decoration: InputDecoration(
                contentPadding: EdgeInsets.symmetric(vertical: 15.0),
                fillColor: Colors.white,
                filled: true,
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(15.0),
                  borderSide: BorderSide(width: 0.8),
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(15.0),
                  borderSide: BorderSide(
                    width: 0.8,
                    color: Theme.of(context).primaryColor,
                  ),
                ),
                hintText: 'Search',
                prefixIcon: Icon(
                  Icons.search,
                  size: 30.0,
                  color: Colors.green,
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment
                .topCenter, // Align however you like (i.e .centerRight, centerLeft)
            child: Text("Danh sách có 1314 đơn"),
          ),
          Container(
            height: 500.0,
            child: ListView(
              scrollDirection: Axis.vertical,
              children: <Widget>[
                GestureDetector(
                  onTap: () => showModalBottomSheet<void>(
                    context: context,
                    isScrollControlled: true,                   
                    builder: (BuildContext context) {                  
                     
                    },
                    
                  ),
                  child: Card(
                    clipBehavior: Clip.antiAlias,
                    child: Column(
                      children: [
                        ListTile(
                          title: const Text(
                            "Đã hủy",
                            style: TextStyle(
                              fontSize: 13.0,
                              backgroundColor: Colors.grey,
                            ),
                          ),
                          leading: const Text(
                            '1.D032111003',
                            style: TextStyle(
                              fontSize: 18.0,
                            ),
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Padding(
                              padding: EdgeInsets.fromLTRB(1.0, 0.0, 0.0, 10.0),
                              child: Padding(
                                padding:
                                    EdgeInsets.fromLTRB(0.0, 0.0, 50.0, 5.0),
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: [
                                    Text('CTV Admitrator 23'),
                                    SizedBox(height: 5.0),
                                    Text('2x UB Trắng gót'),
                                  ],
                                ),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.fromLTRB(50.0, 0.0, 0.0, 5.0),
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  Text('17:00 11/3/2021'),
                                  SizedBox(height: 5.0),
                                  Text('1.000.000đ'),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                Card(
                  clipBehavior: Clip.antiAlias,
                  child: Column(
                    children: [
                      ListTile(
                        title: const Text(
                          "Đã hủy",
                          style: TextStyle(
                            fontSize: 13.0,
                            backgroundColor: Colors.grey,
                          ),
                        ),
                        leading: const Text(
                          '2.D032111003',
                          style: TextStyle(
                            fontSize: 18.0,
                          ),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Padding(
                            padding: EdgeInsets.fromLTRB(1.0, 0.0, 0.0, 10.0),
                            child: Padding(
                              padding: EdgeInsets.fromLTRB(0.0, 0.0, 50.0, 5.0),
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  Text('CTV Admitrator 23'),
                                  SizedBox(height: 5.0),
                                  Text('2x UB Trắng gót'),
                                ],
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.fromLTRB(50.0, 0.0, 0.0, 5.0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Text('17:00 11/3/2021'),
                                SizedBox(height: 5.0),
                                Text('1.000.000đ'),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Card(
                  clipBehavior: Clip.antiAlias,
                  child: Column(
                    children: [
                      ListTile(
                        title: const Text(
                          "Đã hủy",
                          style: TextStyle(
                            fontSize: 13.0,
                            backgroundColor: Colors.grey,
                          ),
                        ),
                        leading: const Text(
                          '2.D032111003',
                          style: TextStyle(
                            fontSize: 18.0,
                          ),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Padding(
                            padding: EdgeInsets.fromLTRB(1.0, 0.0, 0.0, 10.0),
                            child: Padding(
                              padding: EdgeInsets.fromLTRB(0.0, 0.0, 50.0, 5.0),
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  Text('CTV Admitrator 23'),
                                  SizedBox(height: 5.0),
                                  Text('2x UB Trắng gót'),
                                ],
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.fromLTRB(50.0, 0.0, 0.0, 5.0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Text('17:00 11/3/2021'),
                                SizedBox(height: 5.0),
                                Text('1.000.000đ'),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Card(
                  clipBehavior: Clip.antiAlias,
                  child: Column(
                    children: [
                      ListTile(
                        title: const Text(
                          "Đã hủy",
                          style: TextStyle(
                            fontSize: 13.0,
                            backgroundColor: Colors.grey,
                          ),
                        ),
                        leading: const Text(
                          '2.D032111003',
                          style: TextStyle(
                            fontSize: 18.0,
                          ),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Padding(
                            padding: EdgeInsets.fromLTRB(1.0, 0.0, 0.0, 10.0),
                            child: Padding(
                              padding: EdgeInsets.fromLTRB(0.0, 0.0, 50.0, 5.0),
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  Text('CTV Admitrator 23'),
                                  SizedBox(height: 5.0),
                                  Text('2x UB Trắng gót'),
                                ],
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.fromLTRB(50.0, 0.0, 0.0, 5.0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Text('17:00 11/3/2021'),
                                SizedBox(height: 5.0),
                                Text('1.000.000đ'),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Card(
                  clipBehavior: Clip.antiAlias,
                  child: Column(
                    children: [
                      ListTile(
                        title: const Text(
                          "Đã hủy",
                          style: TextStyle(
                            fontSize: 13.0,
                            backgroundColor: Colors.grey,
                          ),
                        ),
                        leading: const Text(
                          '2.D032111003',
                          style: TextStyle(
                            fontSize: 18.0,
                          ),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Padding(
                            padding: EdgeInsets.fromLTRB(1.0, 0.0, 0.0, 10.0),
                            child: Padding(
                              padding: EdgeInsets.fromLTRB(0.0, 0.0, 50.0, 5.0),
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  Text('CTV Admitrator 23'),
                                  SizedBox(height: 5.0),
                                  Text('2x UB Trắng gót'),
                                ],
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.fromLTRB(50.0, 0.0, 0.0, 5.0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Text('17:00 11/3/2021'),
                                SizedBox(height: 5.0),
                                Text('1.000.000đ'),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Card(
                  clipBehavior: Clip.antiAlias,
                  child: Column(
                    children: [
                      ListTile(
                        title: const Text(
                          "Đã hủy",
                          style: TextStyle(
                            fontSize: 13.0,
                            backgroundColor: Colors.grey,
                          ),
                        ),
                        leading: const Text(
                          '2.D032111003',
                          style: TextStyle(
                            fontSize: 18.0,
                          ),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Padding(
                            padding: EdgeInsets.fromLTRB(1.0, 0.0, 0.0, 10.0),
                            child: Padding(
                              padding: EdgeInsets.fromLTRB(0.0, 0.0, 50.0, 5.0),
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  Text('CTV Admitrator 23'),
                                  SizedBox(height: 5.0),
                                  Text('2x UB Trắng gót'),
                                ],
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.fromLTRB(50.0, 0.0, 0.0, 5.0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Text('17:00 11/3/2021'),
                                SizedBox(height: 5.0),
                                Text('1.000.000đ'),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }