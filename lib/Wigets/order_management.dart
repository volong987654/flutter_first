import 'package:backend1/Wigets/Customder.dart';
import 'package:flutter/material.dart';

class order_management extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        brightness: Brightness.light,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(
          color: Theme.of(context).primaryColor,
        ),
        title: Center(
          child: Text(
            'Quản lí đơn hàng',
            style: TextStyle(
              color: Theme.of(context).primaryColor,
              fontSize: 20.0,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        actions: <Widget>[
          FlatButton(
            child: Row(
              children: [
                Icon(Icons.shopping_cart),
              ],
            ),
            onPressed: () {},
          )
        ],
      ),
      drawer: CustomDrawer(),
      body: ListView(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(5.0),
            child: TextField(
              decoration: InputDecoration(
                contentPadding: EdgeInsets.symmetric(vertical: 15.0),
                fillColor: Colors.white,
                filled: true,
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(15.0),
                  borderSide: BorderSide(width: 0.8),
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(15.0),
                  borderSide: BorderSide(
                    width: 0.8,
                    color: Theme.of(context).primaryColor,
                  ),
                ),
                hintText: 'Search',
                prefixIcon: Icon(
                  Icons.search,
                  size: 30.0,
                  color: Colors.green,
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment
                .topCenter, // Align however you like (i.e .centerRight, centerLeft)
            child: Text("Danh sách có 8 đơn"),
          ),
         SingleChildScrollView(
           child: Container(
            //  width: MediaQuery.of(context).size.width,
            //  height: MediaQuery.of(context).size.height,
            height: 300.0,
             child: ListView(
               children: <Widget>[
                 for(int i=1;i<10;i++)
                 Card(
                    child: Column(
                      children: [
                       Row(
                         mainAxisAlignment: MainAxisAlignment.spaceBetween,
                         children: [
                        Row(
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: Text('1.D102002001'),
                            ),
                            Text('Hết hàng',style: TextStyle(backgroundColor: Colors.redAccent),),
                          ],
                        ),
                           Padding(
                             padding: const EdgeInsets.all(5.0),
                             child: Text('VTD022022101',style: TextStyle(color: Colors.red),),
                           ),
                         ],
                       ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            
                               
                                 Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.all(5.0),
                                      child: Text('CTV Tài Khoản Test App 1'),
                                    ),
                                    SizedBox(height: 5.0),
                                     Padding(
                                       padding: const EdgeInsets.all(5.0),
                                       child: Text('Jodan chicago đen đỏ(36)'),
                                     )
                                  ],
                                ),
                              
                            
                            Padding(
                              padding: EdgeInsets.fromLTRB(50.0, 0.0, 0.0, 5.0),
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.all(5.0),
                                    child: Text('17:00 11/3/2021'),
                                  ),
                                  SizedBox(height: 5.0),
                                  Text('1.000.000đ'),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                 ),
               ],
             ),
             ),
             ),
          
        ],
      ),
    );
  }
}