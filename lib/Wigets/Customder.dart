import 'package:backend1/Logins/Login_Screen.dart';
import 'package:backend1/Wigets/Order.dart';

import 'package:backend1/Wigets/Product%20_management.dart';
import 'package:backend1/Wigets/draft_menu.dart';
import 'package:backend1/Wigets/myAccount.dart';
import 'package:backend1/Wigets/order_management.dart';
import 'package:backend1/Wigets/processed.dart';
import 'package:flutter/material.dart';

class CustomDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          Stack(
            children: <Widget>[
              Image(
                  height: 80.0,
                  width: double.infinity,
                  image: AssetImage(
                    "assets/images/nen.jpg",
                  ),
                  fit: BoxFit.cover),
            ],
          ),
          Align(
            alignment: Alignment
                .centerLeft, // Align however you like (i.e .centerRight, centerLeft)
            child: Text("Phiên bản ứng dụng 1.8.13"),
          ),
          Align(
            alignment: Alignment
                .centerLeft, // Align however you like (i.e .centerRight, centerLeft)
            child: Text("Xin chào Admitractor23"),
          ),
          Container(
            height: 400.0,
            child: ListView(
              scrollDirection: Axis.vertical,
              children: <Widget>[
                CustomListTile(
                    'Tài khoản của tôi',
                    '',
                    () => {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => myAccount()))
                        }),
                CustomListTile(
                    'Tất cả sản phẩm',
                    '',
                    () => {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => Product_management()))
                        }),
                CustomListTile('Đơn nháp', '(3)', () => { Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => draft_menu()))}),
                CustomListTile('Quản lí đơn hàng', '(3)', ( ) => {Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => order_management()))}),
      
                CustomListTile('Đơn sỉ', '(8)', () => { Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => Order()))}),
                CustomListTile('Scan lên xe', '', () => {}),
                CustomListTile('Scan tùy chỉnh', '', () => {}),
                CustomListTile('Đơn đang xử lí', '(262)', () => {Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => processed()))}),
                CustomListTile('Scan đơn hoàn', '', () => {}),
                CustomListTile('Quản lí chung', '(10)', () => { }),
                CustomListTile('long', '(10)', () => {}),
                CustomListTile('long', '(10)', () => {}),
                CustomListTile('long', '(10)', () => {}),
                CustomListTile('long', '(10)', () => {}),
                CustomListTile('long', '(10)', () => {}),
                CustomListTile('long', '(10)', () => {}),
                CustomListTile('long', '(10)', () => {}),
                CustomListTile('long', '(10)', () => {}),
               
              ],
            ),
          ),
          GestureDetector(
            onTap: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (_) => LoginScreen(),
              ),
            ),
            // kích vào nút login thì chuyển trang

            child: Padding(
              padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 10.0),
              child: Padding(
                padding: EdgeInsets.fromLTRB(0.0, 30.0, 0.0, 0.0),
                child: Container(
                  margin: EdgeInsets.symmetric(horizontal: 0.0),
                  alignment: Alignment.center,
                  height: 40.0,
                  width: 200.0,
                  decoration: BoxDecoration(
                    color: Colors.yellow,
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            'Đăng xuất',
                            style: TextStyle(
                              fontSize: 15.0,
                            ),
                          )),
                      Icon(
                        Icons.logout,
                        color: Colors.black,
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class CustomListTile extends StatelessWidget {
  @override
  String text;
  String text1;
  Function onTap;
  CustomListTile(this.text, this.text1, this.onTap);
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(8.0, 0, 8.0, 0),
      child: Container(
        decoration: BoxDecoration(
            border: Border(bottom: BorderSide(color: Colors.grey))),
        child: InkWell(
          onTap: onTap,
          child: Padding(
            padding: EdgeInsets.all(10.0),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    text,
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  Text(text1),
                ]),
          ),
        ),
      ),
    );
  }
}
