import 'package:backend1/Wigets/Customder.dart';
import 'package:flutter/material.dart';

class Product_management extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        brightness: Brightness.light,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(
          color: Theme.of(context).primaryColor,
        ),
        title: Text(
          'Quản lý sản phẩm',
          style: TextStyle(
            color: Theme.of(context).primaryColor,
            fontWeight: FontWeight.bold,
          ),
        ),
        actions: <Widget>[
          FlatButton(
            child: Row(
              children: [Icon(Icons.filter_alt_outlined, size: 30.0)],
            ),
            onPressed: () {},
          )
        ],
      ),
      drawer: CustomDrawer(),
      body: ListView(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(5.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(
                  flex: 8,
                  child: TextField(
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.symmetric(vertical: 15.0),
                      fillColor: Colors.white,
                      filled: true,
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(15.0),
                        borderSide: BorderSide(width: 0.8),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(15.0),
                        borderSide: BorderSide(
                          width: 0.8,
                          color: Theme.of(context).primaryColor,
                        ),
                      ),
                      hintText: 'Search',
                      prefixIcon: Icon(
                        Icons.search,
                        size: 30.0,
                        color: Colors.green,
                      ),
                    ),
                  ),
                ),
                Padding(padding: EdgeInsets.only(right: 5)),
                Container(
                  height: 50,
                  width: 50,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    color: Colors.lightBlue,
                  ),
                  child: Icon(
                    Icons.add_box_outlined,
                    color: Colors.white,
                  ),
                ),
              ],
            ),
          ),
          Align(
            alignment: Alignment
                .topCenter, // Align however you like (i.e .centerRight, centerLeft)
            child: Text("Danh sách có 1314 đơn"),
          ),
         SingleChildScrollView(
           child:  Container(
             width: MediaQuery.of(context).size.width,
             height: MediaQuery.of(context).size.height,

            
            child: ListView(
              scrollDirection: Axis.vertical,
              children: <Widget>[
                for (int i=1; i <= 5; i++)
                  Row(
                    children: <Widget>[
                      Expanded(
                        flex: 5,
                        child: Card(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                '1.van Mickey trắng',
                                textAlign: TextAlign.end,
                                style: TextStyle(
                                  fontSize: 15.0,
                                  color: Colors.red,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Expanded(
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: [
                                        GestureDetector(
  onTap: () {
    print('đã click');
  },
  child: Image.asset(
    'assets/images/sanpham.PNG',
    fit: BoxFit.cover, 
  ),
)
                                      ],
                                    ),
                                  ),
                                  Container(
                                    margin:
                                        EdgeInsets.fromLTRB(0.0, 1.0, 0.0, 0.0),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Wrap(
                                          crossAxisAlignment:
                                              WrapCrossAlignment.center,
                                          children: [
                                            Icon(Icons.people),
                                            Text('190.000đ'),
                                          ],
                                        ),
                                        SizedBox(height: 5.0),
                                        Wrap(
                                          crossAxisAlignment:
                                              WrapCrossAlignment.center,
                                          children: [
                                            Icon(Icons.account_circle),
                                            Text('190.000đ'),
                                          ],
                                        ),
                                        Wrap(
                                          crossAxisAlignment:
                                              WrapCrossAlignment.center,
                                          children: [
                                            Icon(Icons.account_circle),
                                            Text('190.000đ'),
                                          ],
                                        ),
                                        Wrap(
                                          children: [
                                            Padding(
                                              padding: EdgeInsets.all(5.0),
                                              child: Text('Free ship',
                                                  style: TextStyle(
                                                    backgroundColor:
                                                        Colors.greenAccent,
                                                  )),
                                            )
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 5,
                        child: Card(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                '1.van Mickey trắng',
                                textAlign: TextAlign.end,
                                style: TextStyle(
                                  fontSize: 15.0,
                                  color: Colors.red,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    child: Expanded(
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceEvenly,
                                        children: [
                                          Image.asset(
                                            "assets/images/sanpham.PNG",
                                            fit: BoxFit.contain,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  Container(
                                    margin:
                                        EdgeInsets.fromLTRB(0.0, 1.0, 0.0, 0.0),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Wrap(
                                          crossAxisAlignment:
                                              WrapCrossAlignment.center,
                                          children: [
                                            Icon(Icons.people),
                                            Text('190.000đ'),
                                          ],
                                        ),
                                        SizedBox(height: 5.0),
                                        Wrap(
                                          crossAxisAlignment:
                                              WrapCrossAlignment.center,
                                          children: [
                                            Icon(Icons.account_circle),
                                            Text('190.000đ'),
                                          ],
                                        ),
                                        Wrap(
                                          crossAxisAlignment:
                                              WrapCrossAlignment.center,
                                          children: [
                                            Icon(Icons.account_circle),
                                            Text('190.000đ'),
                                          ],
                                        ),
                                        Wrap(
                                          children: [
                                            Padding(
                                              padding: EdgeInsets.all(5.0),
                                              child: Text('Free ship',
                                                  style: TextStyle(
                                                    backgroundColor:
                                                        Colors.greenAccent,
                                                  )),
                                            )
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
              ],
            ),
          ),)
        ],
      ),
    );
  }
}
