import 'package:flutter/material.dart';

class myAccount extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        brightness: Brightness.light,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(
          color: Theme.of(context).primaryColor,
        ),
        title: Text(
          'Tài Khoản Của Tôi',
          style: TextStyle(
            color: Theme.of(context).primaryColor,
            fontSize: 20.0,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      body: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(15.0),
            child: Align(
              alignment: Alignment
                  .centerLeft, // Align however you like (i.e .centerRight, centerLeft)
              child: Text("Thông tin chung"),
            ),
          ),
          Card(
            clipBehavior: Clip.antiAlias,
            child: Column(
              children: [
                CustomListTile('Tài khoản', 'Admintrator', () => {}),
                CustomListTile('Hạng thành viên', 'Quản trị viên', () => {}),
                CustomListTile('Điểm tích lũy', '-80', () => {}),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(15.0),
            child: Align(
              alignment: Alignment
                  .centerLeft, // Align however you like (i.e .centerRight, centerLeft)
              child: Text("Cập nhật thông tin"),
            ),
          ),
           Container(
          height: 300.0,
          
          child: ListView(
            scrollDirection: Axis.vertical,
            children: <Widget>[
             Card(
                clipBehavior: Clip.antiAlias,
                child: Column(
                  children: [
                    ListTile(
                      leading: Icon(Icons.account_circle,color: Colors.green),
                      title: const Text('Admintrtor23',style: TextStyle(
                        fontSize: 15.0,
                      ),),
                    ),
                  ],
                ),
              ),   
              SizedBox(height: 10.0),                
             Card(
                clipBehavior: Clip.antiAlias,
                child: Column(
                  children: [
                    ListTile(
                      leading: Icon(Icons.phone,color: Colors.green),
                      title: const Text('0927267496',style: TextStyle(
                        fontSize: 15.0,
                      ),),
                    ),
                  ],
                ),
              ),
               SizedBox(height: 10.0),
             Card(
                clipBehavior: Clip.antiAlias,
                child: Column(
                  children: [
                    ListTile(
                      leading: Icon(Icons.mail,color: Colors.green),
                      title: const Text('volong987654@gmail.com',
                      style: TextStyle(
                        fontSize: 15.0,
                      ),),
                    ),
                  ],
                ),
              ),
               SizedBox(height: 10.0),
             Card(
                clipBehavior: Clip.antiAlias,
                child: Column(
                  children: [
                    ListTile(
                      leading: Icon(Icons.arrow_drop_down_circle,color: Colors.green),
                      title: const Text('1231243212',style: TextStyle(
                        fontSize: 15.0,
                      ),),
                    ),
                  ],
                ),
              ),
             Card(
                clipBehavior: Clip.antiAlias,
                child: Column(
                  children: [
                    Padding(
                padding: EdgeInsets.symmetric(horizontal: 0.0, vertical:0.0),
                child: TextField(
                  decoration: InputDecoration(
                    
                    fillColor: Colors.white,
                    filled: true,
                    hintText: 'số tài khoản ngân hàng',
                    prefixIcon: Icon(
                      Icons.money,
                      size: 20.0,
                      color: Colors.green
                    ),
                  ),
                ),
              ),
                  ],
                ),
              ),
             Card(
                clipBehavior: Clip.antiAlias,
                child: Column(
                  children: [
                    Padding(
                padding: EdgeInsets.symmetric(horizontal: 0.0, vertical:0.0),
                child: TextField(
                  decoration: InputDecoration(
                    
                    fillColor: Colors.white,
                    filled: true,
                    hintText: 'Tên Ngân Hàng',
                    prefixIcon: Icon(
                      Icons.add_business_sharp,
                      size: 20.0,
                      color: Colors.green
                    ),
                  ),
                ),
              ),
                  ],
                ),
              ),
             Card(
                clipBehavior: Clip.antiAlias,
                child: Column(
                  children: [
                    Padding(
                padding: EdgeInsets.symmetric(horizontal: 0.0, vertical:0.0),
                child: TextField(
                  decoration: InputDecoration(
                    
                    fillColor: Colors.white,
                    filled: true,
                    hintText: 'Đổi mật khẩu',
                    prefixIcon: Icon(
                      Icons.lock,
                      size: 20.0,
                      color: Colors.green
                    ),
                  ),
                ),
              ),
                  ],
                ),
              ),
             Card(
                clipBehavior: Clip.antiAlias,
                child: Column(
                  children: [
                    Padding(
                padding: EdgeInsets.symmetric(horizontal: 0.0, vertical:0.0),
                child: TextField(
                  decoration: InputDecoration(                  
                             
                  
                    fillColor: Colors.white,
                    filled: true,
                    hintText: 'Nhập lại mật khẩu',
                    prefixIcon: Icon(
                      Icons.lock,
                      size: 20.0,
                      color: Colors.green
                    ),
                  ),
                ),
              ),
                  ],
                ),
              ),
              
            ],
          ),
          ),
        ],
      ),
    );
  }
}

class CustomListTile extends StatelessWidget {
  @override
  String text;
  String text1;
  Function onTap;
  CustomListTile(this.text, this.text1, this.onTap);
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(8.0, 0, 8.0, 0),
      child: Container(
        decoration: BoxDecoration(
            border: Border(bottom: BorderSide(color: Colors.grey))),
        child: InkWell(
          onTap: onTap,
          child: Padding(
            padding: EdgeInsets.all(10.0),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    text,
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  Text(text1),
                ]),
          ),
        ),
      ),
    );
  }
}
