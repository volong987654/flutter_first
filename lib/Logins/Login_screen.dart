
import 'package:backend1/Logins/home_screen.dart';
import 'package:flutter/material.dart';


class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
                    SizedBox(
                      height: 155.0,
                      child: Image.asset(
                        "assets/images/logo.png",
                        fit: BoxFit.contain,
                      ),
                    ),
              Text(
                'Đăng nhập hệ thống',
                style: TextStyle(
                  color: Colors.green,
                  fontSize: 18.0,
                  fontWeight: FontWeight.bold,
                  letterSpacing: 1.0,
                ),
              ),
              Container(
                width: 500,
                height: 100,
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
                  child: TextField(
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.symmetric(vertical: 15.0),
                       border:
                    OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
                      fillColor: Colors.white,
                      filled: true,
                      hintText: 'Tên đăng nhập',
                      hintStyle: TextStyle(fontSize: 18.0),
                      prefixIcon: Icon(
                        Icons.account_box,
                        size: 20.0,
                        color: Colors.green,
                      ),
                    ),
                  ),
                ),
              ),
              Container(
                width: 500,
                height: 100,
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
                  child: TextField(
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.symmetric(vertical: 15.0),
                     border:
                    OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
                      fillColor: Colors.white,
                      filled: true,
                      hintText: 'Mật khẩu',
                      hintStyle: TextStyle(fontSize: 18.0),
                      prefixIcon: Container(
                        padding:EdgeInsets.only(bottom:2),
                        child: Icon(
                          Icons.lock,
                          size: 20.0,
                          color: Colors.green,
                        ),
                      ),
                    ),
                    obscureText: true, // khi đáh mật khẩu thì ko hiện
                  ),
                ),
              ),
              SizedBox(height: 40.0),
              GestureDetector(
                onTap: ()  =>Navigator.push(
                  context,
                   MaterialPageRoute(
                     builder:(_) => HomeScreen(),
                   ),     

                ), // kích vào nút login thì chuyển trang 
                child: Container(
                  margin: EdgeInsets.symmetric(horizontal: 60.0),
                  alignment: Alignment.center,
                  height: 30.0,
                  width: 130.0,
                  decoration: BoxDecoration(
                    color: Colors.green,
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  child: Text(
                    'Đăng nhập',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 18.0,
                      fontWeight: FontWeight.w600,
                      letterSpacing: 1.5,
                    ),
                  ),
                  
                ),
                
              ),
                SizedBox(height: 40.0),
              Text(
                'Đăng ký cộng tác viên',
                style: TextStyle(
                  color: Colors.green,
                  fontSize: 15.0,
                  fontWeight: FontWeight.w500,
                  letterSpacing: 1.0,
                ),
              ),
                SizedBox(height: 40.0),
              Text(
                'Quên mật khẩu ?',
                style: TextStyle(
                  color: Colors.green,
                  fontSize: 15.0,
                  fontWeight: FontWeight.w500,
                  letterSpacing: 1.0,
                ),
              ),
            ],
            
          ),
          
        ),
      ),
      
    );
  }
}
